package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;

import static com.epam.rd.java.basic.task7.db.SQLQuery.*;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {

    }

    public Connection getConnection() throws SQLException {
        String URL = null;
        try (InputStream input = new FileInputStream("app.properties")) {
            Properties properties = new Properties();
            properties.load(input);
            URL = properties.getProperty("connection.url");
            if (URL != null) {
                return DriverManager.getConnection(URL);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new SQLException();
    }


    public List<User> findAllUsers() throws DBException {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();
        try {
            conn = instance.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(SELECT_ALL_USERS);
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(1));
                user.setLogin(rs.getString(2));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException("something went wrong with selecting all users", e);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = instance.getConnection();
            stmt = conn.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getLogin());
            if (stmt.executeUpdate() == 0) return false;
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }
            return true;
        } catch (SQLException e) {
            throw new DBException("Something went wrong with user insertion", e);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        int count = 0;
        try {
            connection = instance.getConnection();
            for (User user : users) {
                ps = connection.prepareStatement(DELETE_USER);
                ps.setInt(1, user.getId());
                count += ps.executeUpdate();
            }
            if (count > 0) return true;
        } catch (SQLException e) {
            throw new DBException("something went wrong with users deleting", e);
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        User user = null;
        try {
            conn = instance.getConnection();
            stmt = conn.prepareStatement(SELECT_USER_BY_LOGIN);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt(1));
                user.setLogin(rs.getString(2));
            }
        } catch (SQLException e) {
            throw new DBException("Something went wrong with user getting", e);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Team team = null;
        try {
            conn = instance.getConnection();
            stmt = conn.prepareStatement(SELECT_TEAM_BY_NAME);
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            while (rs.next()) {
                team = new Team();
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
            }
        } catch (SQLException e) {
            throw new DBException("Something went wrong with team getting", e);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return team;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = instance.getConnection();
            ps = connection.prepareStatement(UPDATE_TEAM);
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            if (ps.executeUpdate() > 0) return true;
        } catch (SQLException e) {
            throw new DBException("Something went wrong with team updating", e.getCause());
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = instance.getConnection();
            ps = connection.prepareStatement(DELETE_TEAM);
            ps.setInt(1, team.getId());
            if (ps.executeUpdate() > 0) return true;
        } catch (SQLException e) {
            throw new DBException("Something went wrong with team deleting", e.getCause());
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = instance.getConnection();
            stmt = conn.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, team.getName());
            int c = stmt.executeUpdate();
            if (c == 0) return false;
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new DBException("Something went wrong with team insertion", e);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return true;
    }

    public List<Team> findAllTeams() throws DBException {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();
        try {

            conn = instance.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(SELECT_ALL_TEAMS);
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("Something went wrong team selecting", e);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return teams;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = instance.getConnection();
            connection.setAutoCommit(false);

            for (Team team : teams) {
                ps = connection.prepareStatement(INSERT_TEAMS_FOR_USERS);
                ps.setInt(1, user.getId());
                ps.setInt(2, team.getId());
                ps.executeUpdate();
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            rollback(connection);
            throw new DBException("Something went wrong with setting teams for user", e);
        } finally {
            close(ps);
            close(connection);

        }
    }


    public List<Team> getUserTeams(User user) throws DBException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();
        try {
            conn = instance.getConnection();
            stmt = conn.prepareStatement(SELECT_TEAM_BY_USER);
            stmt.setInt(1, user.getId());
            rs = stmt.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("Something went wrong with getting user teams ", e.getCause());
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return teams;
    }

    private void rollback(Connection conn) {
        try {
            conn.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
