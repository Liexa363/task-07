package com.epam.rd.java.basic.task7.db;

public final class SQLQuery {
    public static final String SELECT_ALL_USERS = "SELECT * FROM users";
    public static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    public static final String SELECT_USER_BY_LOGIN ="SELECT * FROM users WHERE login=?";
    public static final String INSERT_USER = "INSERT INTO users values (DEFAULT,?)";
    public static final String DELETE_USER = "delete from users where id=?";

    public static final String SELECT_TEAM_BY_NAME ="SELECT * FROM teams WHERE name=?";
    public static final String SELECT_TEAM_BY_USER ="SELECT * FROM teams WHERE id in " +
            "(select team_id from users_teams where user_id in " +
            "(select id from users where id=?))";

    public static final String INSERT_TEAM = "INSERT INTO teams values (DEFAULT,?)";
    public static final String DELETE_TEAM = "DELETE from teams WHERE id=?";
    public static final String UPDATE_TEAM = "UPDATE teams " +
            "SET name = ? " +
            "WHERE id = ?";

    public static final String INSERT_TEAMS_FOR_USERS = "INSERT INTO users_teams values (?,?)";

}
